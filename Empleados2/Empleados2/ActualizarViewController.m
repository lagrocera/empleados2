//
//  ActualizarViewController.m
//  Empleados
//
//  Created by Yeffers23 on 5/6/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "ActualizarViewController.h"
#import "Empleados.h"

@interface ActualizarViewController (){

    Empleados *actualizarEmpleado;
    Empleados *buscarEmpleado;
    
}

@end

@implementation ActualizarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    actualizarEmpleado = [[Empleados alloc]init];
    buscarEmpleado = [[Empleados alloc]init];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)buscarbutton:(id)sender {
    buscarEmpleado.empCedula = _cedulatxt.text;
    [buscarEmpleado searchEmployedInDataBasebyId];
    
    _cedulatxt.text=buscarEmpleado.empId;
    _nombreTxt.text=buscarEmpleado.empName;
    _direccionTxt.text=buscarEmpleado.empAdress;
    _edadTxt.text=buscarEmpleado.empAge;
    
    _labelStatus.text=buscarEmpleado.status;
}

- (IBAction)actualizarbutton:(id)sender {
    
    
    actualizarEmpleado.empName = _nombreTxt.text;
    actualizarEmpleado.empCedula=_cedulatxt.text;
    actualizarEmpleado.empAdress= _direccionTxt.text;
    actualizarEmpleado.empAge=_edadTxt.text;
    actualizarEmpleado.empId = buscarEmpleado.empId;
    
    [actualizarEmpleado updateEmployedInDatabase];
    
    _labelStatus.text = actualizarEmpleado.status;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
    
}

@end
