//
//  RegistrarEmpleadoViewController.h
//  Empleados
//
//  Created by centro docente de computos on 6/05/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Empleados.h"

@interface RegistrarEmpleadoViewController : UIViewController
{
    //propiedad empleado-objeto empleado
    Empleados *nuevoEmpleado;
}
@property (weak, nonatomic) IBOutlet UITextField *empCedulaNew;
@property (weak, nonatomic) IBOutlet UITextField *empNameNew;
@property (weak, nonatomic) IBOutlet UITextField *empAdressNew;
@property (weak, nonatomic) IBOutlet UITextField *empAgeNew;
@property (weak, nonatomic) IBOutlet UILabel *statusText;

- (IBAction)saveEmp:(id)sender;
@end
