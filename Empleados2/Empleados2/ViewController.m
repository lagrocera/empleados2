//
//  ViewController.m
//  Empleados2
//
//  Created by S209e19 on 16/10/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "ViewController.h"
#import "Empleados.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    Empleados * emp = [[Empleados alloc]init];
    [emp createDatabaseInDocuments];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
    
}

@end
